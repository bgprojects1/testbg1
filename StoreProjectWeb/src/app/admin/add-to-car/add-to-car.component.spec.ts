import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddToCarComponent } from './add-to-car.component';

describe('AddToCarComponent', () => {
  let component: AddToCarComponent;
  let fixture: ComponentFixture<AddToCarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddToCarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddToCarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
