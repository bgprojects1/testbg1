import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faMinus } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-add-to-car',
  templateUrl: './add-to-car.component.html',
  styleUrls: ['./add-to-car.component.css']
})
export class AddToCarComponent {

  faPlus = faPlus;
  faMinus = faMinus;

  products: any;
  selectedItems: any[] = [];


  constructor(private router: Router) {
    this.products = [
      {
        id: 0,
        name: 'Licuadora 4 velocidades',
        description:
          'Producto de mejor calidad con todas las herramientas necesarias para un completo menejo',
        value: 150.24,
        rating: '1.5',
        image: 'product-1.png',
      },
      {
        id: 1,
        name: 'COSORI Olla a presión eléctrica',
        description:
          'de 6 cuartos de galón, olla múltiple instantánea 9 en 1, 13 presets, olla de cocción lenta para arroz, salteado, al vacío, esterilizador, recetas, 1100W',
        value: 150.24,
        rating: '0.5',
        image: 'product-2.png',
      },
      {
        id: 2,
        name: 'Combo de hogar 4 en 1',
        description:
          'Combinación de articulos del hogar necesario para todas las actividades diarias de un hogar polifuncional',
        value: 78.00,
        rating: '2.2',
        image: 'product-3.png',
      },
      {
        id: 3,
        name: 'Audifonos MCM FullAudit 2.0',
        description:
          'Optimiza tu rutina de entrenamiento con un puntaje de preparación diaria que revela si estás listo para hacer ejercicio o si debes centrarte en la recuperación',
        value: 274.80,
        rating: '4.7',
        image: 'product-4.png',
      }
    ];
  }

  isSelected(item: any): boolean {
    return this.selectedItems.includes(item);
  }

  toggleSelection(item: any): void {
    const index = this.selectedItems.indexOf(item);
    if (index > -1) {
      this.selectedItems.splice(index, 1);
    } else {
      this.selectedItems.push(item);
    }
  }

  purchase() {
    this.router.navigate(['/tienda/comprar'])
  }

}
