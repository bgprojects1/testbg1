import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddToCarComponent } from "./add-to-car/add-to-car.component";
import { PurchaseComponent } from "./purchase/purchase.component";
import { PlanComponent } from "./plan/plan.component";
import { HomeComponent } from "./home/home.component";


const routes: Routes = [
    { path: 'add-to-car', component: AddToCarComponent },
    { path: 'comprar', component: PurchaseComponent },
    { path: 'planes', component: PlanComponent },
    { path: 'home', component: HomeComponent },
    { path: '**', redirectTo: 'home' }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class AdminRoutingModule { }