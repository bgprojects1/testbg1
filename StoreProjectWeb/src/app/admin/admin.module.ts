import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { LayoutComponent } from './layout/layout.component';
import { AdminRoutingModule } from './admin-route.module';

@NgModule({
  declarations: [
    // LayoutComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
