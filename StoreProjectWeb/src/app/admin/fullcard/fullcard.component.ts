import { Component } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { AuthService } from 'src/app/security/services/auth.service';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-fullcard',
  templateUrl: './fullcard.component.html',
  styleUrls: ['./fullcard.component.css'],
  providers: [DynamicDialogRef]
})
export class FullcardComponent {

  product: any;
  comment: string = '';
  comments: any[] = [];
  hasComment: boolean = false;
  isAuth: boolean = false;

  constructor(
    public config: DynamicDialogConfig, 
    private auth: AuthService,
    private api: ApiService) {
      this.isAuth = this.auth.isLoggedIn();
      this.product = this.config.data;
      const id = this.config.data.id;
      this.getFeedbackInfo(id);
  }

  getFeedbackInfo(id: number) {
    this.api.getProductById(id).subscribe({
      next:(res) => {
        if (res.code === '000') {
          this.product = res.data;
          if (res.data.feedback) {
            this.comments = res.data.feedback.filter((item: any) => item.comment);
            if (this.auth.isLoggedIn()) {
              const userComment = res.data.feedback.find((item: any) => item.customerId === this.auth.getCustomerID() && item.comment);
              if (userComment) this.hasComment = true;
            }
          }
        } else {
          console.log('error');
        }
      },
      error:() => { }
    })
  }

  ngOnInit() {

  }

  addToCard() {
    console.log('add')
  }

  saveComment() {
    const model = {
      'customerId' : +this.auth.getCustomerID(),
      'productId' : +this.product.id,
      'comment' : this.comment,
      'rate' : null,
    }
    this.api.saveFeedback(model).subscribe({
      next:(res) => {
        console.log(res);
        if (res.code === '000') {
          this.getFeedbackInfo(this.product.id);
        } else {
          console.log('error');
        }
      },
      error:() => { }
    })
  }

 

}
