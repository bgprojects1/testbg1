import { Component } from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FullcardComponent } from '../fullcard/fullcard.component';
import { Observable, catchError, map, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { faFilter } from '@fortawesome/free-solid-svg-icons';
import { faArrowDownAZ } from '@fortawesome/free-solid-svg-icons';
import { faArrowUpAZ } from '@fortawesome/free-solid-svg-icons';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [DialogService],
})
export class HomeComponent {
  ref: DynamicDialogRef | undefined;
  
  products: any;
  
  filter: any;
  filterSelected: string = '';

  filterPrice: any;
  filterPriceSelected: string = '';

  faFilter = faFilter;

  constructor(
    public dialogService: DialogService,
    private httpClient: HttpClient,
    private api: ApiService,
  ) {
    this.fillFilters();
    this.getProducts();
  }

  fillFilters() {
    this.filter = [
      {
        icon: faArrowDownAZ,
        justify: 'Justify',
        value: 'DESC',
      },
      {
        icon: faArrowUpAZ,
        justify: 'Justify',
        value: 'ASC',
      },
    ];

    this.filterPrice = [
      {
        icon: faArrowDownAZ,
        justify: 'Justify',
        name: 'mayor precio',
        value: 'PLUS',
      },
      {
        icon: faArrowUpAZ,
        justify: 'Justify',
        name: 'menor precio',
        value: 'LESS',
      },
    ];
  }

  getProducts() {
    this.api.getProducts().subscribe({
      next:(res) => {
        if (res.code === '000') {
          this.products = res.data;          
        } else {
          console.log('error');
        }
      },
      error:() => { }
    })
  }

  getImage(filename: string): Observable<boolean> {
    const folderPath = `assets/images/${filename}`;
    return this.httpClient
      .get(folderPath, { observe: 'response', responseType: 'blob' })
      .pipe(
        map(() => {
          return true;
        }),
        catchError(() => {
          return of(false);
        })
      );
  }

  toggleExpand(product: any) {
    this.ref = this.dialogService.open(FullcardComponent, {
      header: product.name,
      data: product,
      width: '50%',
      height: 'auto',
      contentStyle: { overflow: 'auto' },
      baseZIndex: 10000,
      maximizable: true,
      closable: true
    });
  }

  orderByLetter() {
    this.filterPriceSelected = '';
    if (this.filterSelected === 'ASC') this.products.sort((a: any, b: any) => (a.name > b.name ? 1 : -1));
    else this.products.sort((a: any, b: any) => (a.name > b.name ? -1 : 1));
  }

  orderByPrice() {
    this.filterSelected = '';
    if (this.filterPriceSelected === 'LESS') this.products.sort((a: any, b: any) => (a.value > b.value ? 1 : -1));
    else this.products.sort((a: any, b: any) => (a.value > b.value ? -1 : 1));
  }
  
}
