import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faCartPlus } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent {
  faCartPlus = faCartPlus;
  faUser = faUser;

  constructor(private router: Router, private activeRoute: ActivatedRoute) {

  }

  logout() {
    localStorage.clear();
    // this.router.navigate(['inicio-sesion'])
    this.router.navigate(['inicio-sesion'], { relativeTo: this.activeRoute.parent });
  }
}
