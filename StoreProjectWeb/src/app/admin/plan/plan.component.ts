import { Component } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.css']
})
export class PlanComponent {

  plans: any;

  constructor(private cookieService: CookieService) {
    this.plans = [
      {
        'name': 'Plan Básico',
        'description': 'descripcion de plan basico',
        'value': '563.00',
        'color': '#f47100',
        'code': '0001',
        'icon': '',
      },
      {
        'name': 'Plan Pro',
        'description': 'descripcion de plan pro',
        'value': '900.00',
        'color': '#66BB6A',
        'code': '0002',
        'icon': '',
      },
      {
        'name': 'Plan Premium',
        'description': 'descripcion de plan premium',
        'value': '1500.00',
        'color': '#039BE5',
        'code': '0003',
        'icon': '',
      }
    ]
  }

  savePlan(value: any) {
    this.cookieService.set('planCode', value.code);

    console.log(this.cookieService.get('planCode'));
  }

}
