import { Component, OnInit } from '@angular/core';
import * as Leaflet from 'leaflet';
import { circle, latLng, polygon, tileLayer } from 'leaflet';

var LeafIcon = Leaflet.Icon.extend({
  options: {
     iconSize:     [38, 95],
     shadowSize:   [50, 64],
     iconAnchor:   [22, 94],
     shadowAnchor: [4, 62],
     popupAnchor:  [-3, -76]
  }
});

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.css']
})
export class PurchaseComponent  {

  map!: Leaflet.Map;
  markers: Leaflet.Marker[] = [];
  options = {
    layers: [
      Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      })
    ],
    zoom: 16,
    center: { lat: 28.626137, lng: 79.821603 }
  }

//   iconMark = Leaflet.Icon.extend({
//     options: {
//         shadowUrl: 'leaf-shadow.png',
//         iconSize:     [38, 95],
//         shadowSize:   [50, 64],
//         iconAnchor:   [22, 94],
//         shadowAnchor: [4, 62],
//         popupAnchor:  [-3, -76]
//     }
// });

  // ngOnInit() {
  //   this.map.invalidateSize(); 
  // }

  initMarkers() {
    const initialMarkers = [
      {
        position: { lat: 28.625485, lng: 79.821091 },
        draggable: true
      },
      {
        position: { lat: 28.625293, lng: 79.817926 },
        draggable: false
      },
      {
        position: { lat: 28.625182, lng: 79.81464 },
        draggable: true
      }
    ];
    for (let index = 0; index < initialMarkers.length; index++) {
      const data = initialMarkers[index];
      const marker = this.generateMarker(data, index);
      marker.addTo(this.map).bindPopup(`<b>${data.position.lat},  ${data.position.lng}</b>`);
      this.map.panTo(data.position);
      this.markers.push(marker)
    }
  }

  generateMarker(data: any, index: number) {
  //   var greenIcon = new LeafIcon({
  //     iconUrl: 'http://leafletjs.com/examples/custom-icons/leaf-green.png',
  //     shadowUrl: 'http://leafletjs.com/examples/custom-icons/leaf-shadow.png'
  // })

    return Leaflet.marker(data.position, { draggable: data.draggable })
      .on('click', (event) => this.markerClicked(event, index))
      .on('dragend', (event) => this.markerDragEnd(event, index));
  }

  onMapReady($event: Leaflet.Map) {
    this.map = $event;   
    this.map .setView([-2.191806316, -79.88102316], 15); 
    //this.initMarkers();
  }

  mapClicked($event: any) {
    console.log('evento:', $event);
    console.log('click: ' +$event.latlng.lat, $event.latlng.lng);

    // const positionMarker =  {
    //   position: { lat: +$event.latlng, lng: $event.latlng.lng },
    //   draggable: true
    // };

    for(var i = 0; i < this.markers.length; i++){
      this.map.removeLayer(this.markers[i]);
    };
    
    var icon = Leaflet.icon({
      iconUrl: '/assets/icons/icon_mark.png',
      iconSize: [32, 32]
      }); 

    const marker = Leaflet.marker($event.latlng, { draggable: true, icon: icon })
    .on('click', (event) => this.clickMark());

    marker.addTo(this.map).bindPopup(`<b>${+$event.latlng.lat},  ${$event.latlng.lng}</b>`);

    this.map.panTo($event.latlng);

    this.markers.push(marker);


    // //const marker = this.generateMarker(data, index);
    // marker.addTo(this.map).bindPopup(`<b>${data.position.lat},  ${data.position.lng}</b>`);
    // this.map.panTo(data.position);
    // this.markers.push(marker)
  }

  clickMark() {

  }

  markerClicked($event: any, index: number) {
    console.log($event.latlng.lat, $event.latlng.lng);
  }

  markerDragEnd($event: any, index: number) {
    console.log($event.target.getLatLng());
  } 

  // private map: any;

  // options: any = {
  //   layers: [
  //     tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
  //   ],
  //   zoom: 5,
  //   center: latLng(46.879966, -121.726909)
  // };

  // layersControl = {
  //   baseLayers: {
  //     'Open Street Map': tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' }),
  //     'Open Cycle Map': tileLayer('https://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
  //   },
  //   overlays: {
  //     'Big Circle': circle([ 46.95, -122 ], { radius: 5000 }),
  //     'Big Square': polygon([[ 46.8, -121.55 ], [ 46.9, -121.55 ], [ 46.9, -121.7 ], [ 46.8, -121.7 ]])
  //   }
  // }

  // ngOnInit() {
  //   this.initMap();
  // }

  // initMap() {
  //   this.map = L.map('map').setView([51.505, -0.09], 13);
  //   L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  //     attribution: 'Map data &copy; OpenStreetMap contributors'
  //   }).addTo(this.map);
  //   var circle = L.circle([51.508, -0.11], {
  //     color: 'red',
  //     fillColor: '#f03',
  //     fillOpacity: 0.5,
  //     radius: 500
  // }).addTo(this.map);
  // }

  // // private initMap(): void {
  // //   this.map = L.map('map', {
  // //     center: [ 39.8282, -98.5795 ],
  // //     zoom: 3
  // //   });
  // // }

  // ngAfterViewInit(): void { }

}
