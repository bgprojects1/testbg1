import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './security/pages/login/login.component';
import { HomeComponent } from './admin/home/home.component';
import { AddToCarComponent } from './admin/add-to-car/add-to-car.component';
import { PlanComponent } from './admin/plan/plan.component';
import { PurchaseComponent } from './admin/purchase/purchase.component';
import { LayoutComponent } from './admin/layout/layout.component';

const routes: Routes = [
  // { path: 'layout', component: LayoutComponent },
  // { path: 'add-to-car', component: AddToCarComponent },
  // { path: 'comprar', component: PurchaseComponent },
  // { path: 'planes', component: PlanComponent },
  // { path: 'home', component: HomeComponent },
  { path: 'tienda', 
    component: LayoutComponent,
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
  },
  { path: 'inicio-sesion', component: LoginComponent },
  { path: '**', redirectTo: 'inicio-sesion' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
