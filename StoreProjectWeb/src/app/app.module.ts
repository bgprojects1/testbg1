import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './security/pages/login/login.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DividerModule } from 'primeng/divider';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ButtonModule } from 'primeng/button';
import { PasswordModule } from 'primeng/password';
import { HomeComponent } from './admin/home/home.component';
import { CardModule } from 'primeng/card';
import { FullcardComponent } from './admin/fullcard/fullcard.component';
import { DialogModule } from 'primeng/dialog';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddToCarComponent } from './admin/add-to-car/add-to-car.component';
import { PlanComponent } from './admin/plan/plan.component';
import {CookieService} from 'ngx-cookie-service'; 
import { HttpClientModule } from '@angular/common/http';
import { RatingModule } from 'primeng/rating';
import { DropdownModule } from 'primeng/dropdown';
import { SelectButtonModule } from 'primeng/selectbutton';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ListboxModule } from 'primeng/listbox';
import { CheckboxModule } from 'primeng/checkbox';
import { InputNumberModule } from 'primeng/inputnumber';
import { PurchaseComponent } from './admin/purchase/purchase.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LayoutComponent } from './admin/layout/layout.component';
import { FallbackDirective } from './directives/fallback.directive';
import { ToastModule } from 'primeng/toast';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    FullcardComponent,
    AddToCarComponent,
    PlanComponent,
    PurchaseComponent,
    LayoutComponent,
    FallbackDirective,
    FallbackDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    DividerModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    FontAwesomeModule,
    ButtonModule,
    PasswordModule,
    CardModule,
    DialogModule,
    DynamicDialogModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RatingModule,
    DropdownModule,
    SelectButtonModule,
    InputTextareaModule,
    ListboxModule,
    CheckboxModule,
    InputNumberModule,
    LeafletModule,
    ToastModule,
  ],
  providers: [
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
