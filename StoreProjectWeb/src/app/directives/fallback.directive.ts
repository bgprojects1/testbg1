import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: 'img[fallback]'
  // selector: '[appFallback]'
})
export class FallbackDirective {

  @Input()

  @HostBinding('src')
  src: string = '';
  
  @Input() fallback: string = '';

  @HostListener('error')
  onError() {
    this.src = this.fallback;
  }

  constructor() { }

}
