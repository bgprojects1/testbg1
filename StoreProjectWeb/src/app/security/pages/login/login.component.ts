import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faKey } from '@fortawesome/free-solid-svg-icons';
import { MessageService } from 'primeng/api';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
    providers: [MessageService]
})
export class LoginComponent {

  faUser = faUser;
  faKey = faKey;
  form: FormGroup;

  constructor(
    private router: Router,
    private api: AuthService,
    private formBuilder: FormBuilder,
    private messageService: MessageService) {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  onSubmit() {
    if (this.form.valid) {
      this.api.authenticate(this.form.value).subscribe({
        next:(res) => {
          if (res.code === '000') {
            this.form.reset();
            this.api.saveCustomerProfile(res.data);
            this.messageService.add({ severity: 'success', summary: 'Atención', detail: res.message });
            this.router.navigate(['tienda/home']);
          } else {
            this.messageService.add({ severity: 'error', summary: 'Atención', detail: res.message });
          }
        },
        error:() => {
          this.messageService.add({ severity: 'error', summary: 'Atención', detail: 'Servicio intermitente por favor intentar mas tarde!' });
        }
      })
    } else {
      this.messageService.add({ severity: 'error', summary: 'Atención', detail: 'Campos incompletos por favor verificar' });      
    }
  }

}
