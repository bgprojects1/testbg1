import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseCustomer: string = environment.url + 'customer';

  private isAuthenticated: boolean = false;

  private userId: string = '';

  constructor(private httpClient: HttpClient) { 
    this.loadAuthState(); 
  }

  private loadAuthState() {
    const authState = localStorage.getItem(environment.storageProfile.KEY_PROFILE);
    if (authState) {
      const parsedAuthState = JSON.parse(authState);
      this.userId = parsedAuthState.id;
      this.isAuthenticated = true;
    }
  }

  authenticate(model: any) {
    return this.httpClient.post<any>(`${this.baseCustomer}/authenticate`, model);
  }

  isLoggedIn(): boolean {
    return this.isAuthenticated;
  }

  getCustomerID() {
    return this.userId;
  }

  saveCustomerProfile(customer: any) {    
    this.isAuthenticated = true;
    this.userId = customer.id;
    localStorage.setItem(environment.storageProfile.KEY_PROFILE, JSON.stringify(customer));
  }

  logOut() {
    this.isAuthenticated = false;
    this.userId = '';
    localStorage.clear();
  }

}
