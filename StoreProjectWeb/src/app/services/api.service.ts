import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private baseProduct: string = environment.url + 'product';

  constructor(private httpClient: HttpClient) { }

  getProductById(id: number) {
    return this.httpClient.post<any>(`${this.baseProduct}/obtainProductById`, { 'id': id });
  }

  getProducts() {
    return this.httpClient.get<any>(this.baseProduct);
  }

  saveFeedback(model: any) {
    return this.httpClient.post<any>(`${this.baseProduct}/saveFeedback`, model);
  }
  
}
