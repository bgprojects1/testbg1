export interface ProductI {
    id: number,
    name: string,
    description: string,
    value: number,
    rating: string,
    image: string
}